# README #


###Cloning the Repository
```
	Steps:
		1. cd Desktop or (The path you want to create your folder in)
		3. cd projectName
		4. git clone *Paste your bitbucket link*
```

###Commiting to the Repository
```
	Steps:
		1. cd, until you get into the directory you want.
		2. git add .
		3. git commit -m "*Description of what has been changed/added*"
		4. git push to make the changes
```

###Creating User Name and Email
```
	git config --global user.name "UserName"
	git config --global user.email email@email.com
```
   
   
   
